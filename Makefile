all: cluster deploy grafana prometheus

build:
	@./mvnw clean install dockerfile:build

cluster:
	@oc cluster up --version=v3.7.2 --public-hostname=10.0.2.15
	@oc login -u system:admin

deploy:
	@oc new-project sample
	@oc import-image openjdk18-openshift --from=registry.access.redhat.com/redhat-openjdk-18/openjdk18-openshift --confirm
	@oc new-app -f maven-pipeline.yaml -p APP_NAME=sample-spring-boot -p GIT_SOURCE_URL=https://gitlab.com/Ananasr/sample-spring-boot.git -p GIT_SOURCE_REF=master

grafana:
	@./grafana/setup-grafana.sh -n openshift -p prometheus

prometheus:
	@oc new-project prometheus
	@oc new-app -f https://raw.githubusercontent.com/openshift/origin/release-3.7/examples/prometheus/prometheus.yaml -p NAMESPACE=prometheus

.PHONY: all build cluster deploy grafana prometheus
